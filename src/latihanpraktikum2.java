/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Scanner;
/**
 *
 * @author Alden Dzaky A
 */
public class latihanpraktikum2 {

    /**
     * @param args the command line arguments
     */
  
    private static void tampilJudul(String identitas){
           System.out.println("Identitas : " + identitas);
           System.out.println("\nConvert Kalimat Alay Angka (Vokal Ke Angka)\n");
           
        }
        
           private static String tampilInput(){
             Scanner masuk =  new Scanner (System.in);
             System.out.print("Masukkan Kalimat: ");
             String kalimat = masuk.nextLine();
             System.out.println("Kalimat Asli : " + kalimat);
             return kalimat;
           }
          
           private static String vocal2Angka (String kalimat){
               char[][] arConvert = 
               {{'a','4'},{'i','1'},{'u','2'},{'e','3'},{'o','0'}};
               kalimat = kalimat.toLowerCase();
               for(int i= 0; i<arConvert.length;i++)
                   kalimat= kalimat.replace(arConvert[i][0], arConvert[i][1]);
               return kalimat;
           }
           private static void tampilPerKata(String kalimat, String convert){
               String[] arrKa1 = kalimat.split("  ");
               String[] arrCon = kalimat.split("  ");
               
               for(int i=0;i<arrKa1.length;i++)
                   System.out.println(arrKa1[i]+"=>"+arrCon[i]);
           }
           private static void tampilHasil(String convert) {
               System.out.println("Kalimat Alay Angka: " + convert);
               
                       
           }
        
        public static void main(String[] args) {
        // TODO code application logic here
        String identitas = "Alden Dzaky Aridansyah/XRPL6/05";
        tampilJudul(identitas);
        String kalimat = tampilInput();
        String convert = vocal2Angka(kalimat);
        tampilPerKata(kalimat,convert);
        tampilHasil(convert);
    }
        


    }
